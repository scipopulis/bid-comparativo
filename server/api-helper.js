const ApiHelper = {
    baseAPI: process.env.BASE_API || "http://trancityapi.scipopulis.com/v1/",
    getEndpointConfig: function (key, params) {return this.urlMap(params || {})[key]},
    urlMap: function(obj) {
        let baseAPI = this.baseAPI;
        return {
            'edges_map': {
                method: 'GET',
                contentType: 'text/plain',
                url: generateURL(baseAPI, obj)
            },
            'modals_comparison': {
                method: 'GET',
                contentType: 'text/plain',
                url: generateURL(baseAPI, obj)
            },
        }
    }
};

function generateURL (baseAPI, obj) {
  let url = baseAPI + obj.mapURL + '/' + obj.agency_id
  if (obj.tag) { url = url + '/' + String(obj.tag) }
  return url
}


export default ApiHelper;
