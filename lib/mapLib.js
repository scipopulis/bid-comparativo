Mapa = function () {
    this.map = null;
    this.poly = null;
    this.dirty = false;

    L.Icon.Default.imagePath = 'packages/bevanhunt_leaflet/images';
};

function colorFromSpeed(value, maxValue) {
    if (!value) {
        return "#CCCCCC";
    }

    var hue = 180;
    var sat = 90;
    var light = 50;

    var color = "#000";

    if (value === 0) { //JAM FLAG
        color = "#6b6b6b";
    } else if (value >= 25) {
        color = "#23c932";
    } else if (value >= 20) {
        color = "#f3bd0c";
    } else if (value >= 15) {
        color = "#f35d0c";
    } else if (value < 15) {
        color = "#cc0a0a";
    }

    return color
}


Mapa.prototype.init = function (lat, lng) {
    // console.log("Mapa.prototype.init: ", this);

    var normal = L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png')

    this.map = L.map('map', {
        // maxBounds: [[-23.2475000, -46.4361100], [-23.755000, -46.8361100]],
        minZoom: 3,
        maxZoom: 25,
        zoom: 11,
        layers: normal,
        fullscreenControl: true
    }).setView([lat, lng], 11);

    var baseMaps = {
        "Normal": normal
    };

    L.control.layers(baseMaps).addTo(this.map);

};

Mapa.prototype.clearMap = function () {

    for (var i in this.map._layers) {
        if (this.map._layers[i]._path != undefined) {
            try {
                this.map.removeLayer(this.map._layers[i]);
            }
            catch (e) {
                console.log("problem with " + e + this.map._layers[i]);
            }
        }
    }
};

Mapa.prototype.drawShape = function (shape_vector, curr_speed, maxSpeed) {
    var self = this;
    if (!shape_vector || shape_vector.length <= 1) {
        return;
    }
    for(var i = 1; i < shape_vector.length; i++) {
        var loc1 = shape_vector[i-1].loc;
        var loc2 = shape_vector[i].loc;

        self.drawPolyLine(loc1.lat, loc1.lng, loc2.lat, loc2.lng, { speed: curr_speed, maxSpeed: maxSpeed });
    }
};

Mapa.prototype.drawMarker = function (lat, lng, icon, cetEvent) {
    var self = this;

    var bike = L.icon({
      iconUrl: icon,

      iconSize:     [42, 42], // size of the icon
      shadowSize:   [42, 42], // size of the shadow
      iconAnchor:   [21, 42], // point of the icon which will correspond to marker's location
      shadowAnchor: [21, 42],  // the same for the shadow
      popupAnchor:  [0, -42] // point from which the popup should open relative to the iconAnchor
    });

    var marker=L.marker([lat, lng], {icon: bike})
    marker.addTo(this.map);
    marker.bindPopup("<b>"+cetEvent.info+"</b><br>Última atualização:"+moment(cetEvent.lastUpdate).format('HH:MM'))
};

Mapa.prototype.drawCoordinates = function (coords, speed, maxSpeed) {
    var self = this;
    if (!coords || coords.length <= 1) {
        return;
    }
    for(var i = 1; i < coords.length; i++) {
        var loc1 = coords[i-1];
        var loc2 = coords[i];

        self.drawPolyLine(loc1.lat, loc1.lng, loc2.lat, loc2.lng, { speed: speed, maxSpeed: maxSpeed });
    }
};



Mapa.prototype.drawPolyLine = function (x1, y1, x2, y2, options) {

    var pointA = new L.LatLng(x1, y1);
    var pointB = new L.LatLng(x2, y2);
    var pointList = [pointA, pointB];
    var opacity = options.speed === 0 ? 0.5 : 0.9

    this.polyline = new L.Polyline(pointList, {
        color: colorFromSpeed(options.speed, options.maxSpeed),
        weight: 6,
        opacity: opacity,
        smoothFactor: 1
    });
    this.polyline.addTo(this.map);
};
