if (Meteor.isClient) {

    Template.registerHelper('toFixed', function (x, decimals) {
        return x.toFixed(decimals);
    });

    Template.registerHelper('formatId', function(data) {
        return (data && data._str) || data;
    });

}
