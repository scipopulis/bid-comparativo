Template.footer.helpers({

    formatDateFromNow: function(date) {
        moment.locale('pt');
        return moment(date).fromNow();
    },
  });
