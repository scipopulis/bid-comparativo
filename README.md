# Informações gerais

## Inicialização do projeto
Primeiramente, faz-se a instalação de [Node.js](https://nodejs.org/en/download/), responsável pela execução dos códigos JavaScript.

Feito isso, o repositório deve ser clonado e inicializado fazendo ```meteor run```. Nos casos em que a aplicação sinaliza a ausência de dependências (bibliotecas e outros pacotes), é necessário executar também ```meteor npm install``` para então inicializar de fato o projeto.

Ressalta-se que, neste projeto, a versão em uso é ```1.5.4``` e, por isso, recomenda-se que a instalação seja feita também nessa versão. Mais detalhes sobre versões do framework e sua instalação podem ser verificados [aqui](https://www.meteor.com/developers/install).

Além disso, recomenda-se a instalação do banco de dados MongoDB. Neste projeto, os dados em tempo real provém de API e, portanto, não se utiliza banco de dados para alimentar o painel. No entanto, é possível que novas funcionalidades e/ou informações sejam adicionadas pela agência usuária do código. Nesse contexto, recomenda-se o uso do banco de dados para armazenar informações (estáticas ou dinâmicas) devido à [conexão nativa entre Meteor e Mongo](https://guide.meteor.com/collections.html).

## Configurações adicionais
Para exemplificar as configurações do projeto, este repositório apresenta um arquivo nomeado ```keys.json``` contendo informações privadas como a chave de acesso à API da Scipopulis e a senha de páginas protegidas.

No caso do acesso à API, a chave deve ser disponibilizada pela Scipopulis e ela dará acesso à agência apenas aos dados dentro do seu domínio. Destaca-se que este campo é um requisito para inicializar o projeto.

Para as páginas protegidas (sendo este o caso de São Paulo), a senha pode ser configurada pela própria agência para controle de acesso ao painel.

É importante ressaltar que o arquivo ```keys.json``` não deve ser publicado no repositório do projeto, garantindo que as informações sensíveis se mantenham restritas.

## Deploy
O uso do aplicativo (em produção) requer ainda que sejam especificados o servidor (username e host), diretório do projeto, porta e demais configurações. Recomenda-se o uso do arquivo nomeado como ```setting.json``` para centralizar tais informações, conforme indicado neste repositório.

Quando em produção, recomenda-se ainda o uso do gerenciador de processos [pm2](https://pm2.keymetrics.io/). Dessa forma, a execução do processo pode ser continuamente monitorada, inicializada ou interrompida. As configurações necessárias para tanto estão exemplificadas em ```settings-pm2.json```.

## Contato
fale@scipopulis.com
