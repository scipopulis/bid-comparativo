
set -x

HOST=IP
USER=user
SSH_KEY="~/.ssh/key.key"

SSH_CMD="ssh -i $SSH_KEY  $USER@$HOST"

SRC_DIR='./path/here/bid-comparativo'
BRANCH='master'
EXEC="pm2-meteor deploy"

# -----

$SSH_CMD  "set -x; cd $SRC_DIR && git checkout $BRANCH && git pull && (cd deploy/prod/ && echo 'Deploying $ENV' && $EXEC)"
