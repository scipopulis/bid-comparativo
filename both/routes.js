//Iron.utils.debug = true;

Router.configure({
  layoutTemplate: 'MasterLayout'
});

Router.route('/comparativo/:agency_id', function () {
    const agencyMap = {saopaulo: 'saopaulo_sp', montevideo: 'montevideo_uy'}
    Session.set('agency_id', agencyMap[this.params.agency_id])
    console.log('Agency: ', this.params.agency_id, Session.get('agency_id'))
    this.render('CarBus');
},{
  name: 'onibusxcarro'
});
