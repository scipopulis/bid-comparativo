
Template.carbus_zone.helpers({

    imageForZone: function () {

      const agency_id = Session.get('agency_id');

      if(!this.data.zone) {
        //array trechos sempre terá length = 1
        //(1 trecho/avenida por card, considerando os 2 sentidos de tráfego)
        var regEx = getRegEx(this.data.trechos[0].name);
        return (agency_id + '/' + regEx + '.png');

      }

      if(this.data.zone=='sul') {
        return(agency_id + '/zonasul.png');
      }
      if(this.data.zone=='norte') {
        return(agency_id + '/zonanorte.png');
      }
      if(this.data.zone=='leste') {
        return(agency_id + '/zonaleste.png');
      }
      if(this.data.zone=='oeste') {
        return(agency_id + '/zonaoeste.png');
      }

    },

    getHelpText: function() {
      return "Compara os tempos de<br>viagem atuais de carro<br>e de ônibus em um mesmo<br>trecho";
    },

    classForBar: function (value1, value2) {

        if ((!value1) || (!value2)) {
          return "cinza"
        }

        if(Math.pow((value1-value2),2)<4)
          return "amarelo"

        if (value1 < value2)
            return "verde";

        else
            return "vermelho"
    },

    barSize: function(time1, time2) {
      if(time1>time2)
        return 100;
      else {
        return(Math.round((time1/time2)*100));
      }
    },

    formatTime: function(value) {
      if (!value) return '';
      return(value.toFixed(0));
    }

});

function getRegEx(name) {
  return name.split(" ").slice(1).join('').toLowerCase();
}
