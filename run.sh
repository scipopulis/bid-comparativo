#!/bin/bash

export MONGO_URL=mongodb://localhost:27017/meteor
export MONGO_METEOR_URL=mongodb://localhost:27017/meteor
meteor --port 3022 --settings settings.json
