Session.setDefault('isAvgSelected', true);

Template.MasterLayout.helpers({

  ts: function() {
    var lastUpdate=Session.get('eventLastUpdate');
    return(lastUpdate==null? 0 : lastUpdate);
  },

});

Template.MasterLayout.events({
  'click .tabs li': function(e, t){
    var divId = e.target.id;

    if(e.currentTarget.id=="velmedia") {
      Session.set("isAvgSelected", true);
    } else {
      Session.set("isAvgSelected", false);
    }

    Router.go(divId);
  }
})

Template.MasterLayout.rendered = function() {

  Session.set('eventLastUpdate', new Date());
  window.setInterval(function() {
    Session.set('eventLastUpdate', new Date());
  }, 10*60*1000);

  $("#"+Router.current().route.getName()).addClass('active');
  $('ul.tabs').tabs();
}
