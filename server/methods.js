import { HTTP } from "meteor/http";
import ApiHelper from "./api-helper.js";
var fs = Npm.require('fs');
var keys = JSON.parse(fs.readFileSync(process.cwd() + '/../web.browser/app/keys.json', 'utf8'));

Meteor.methods({
    'get_edges_map': function(agency_id) {
      try {
          let obj = {agency_id: agency_id, mapURL: 'edges_map'}
          let endpoint = ApiHelper.getEndpointConfig("edges_map", obj);
          let result = HTTP.call(endpoint.method, endpoint.url, {headers: {"x-api-key": keys.api_key}});
          return result.data;
      } catch (e) {
          // Got a network error, timeout, or HTTP error in the 400 or 500 range.
          console.error(e)
      }
    },
    'get_modals_comparison': function(agency_id) {
      try {
          let endpoint = ApiHelper.getEndpointConfig("modals_comparison", {agency_id: agency_id, mapURL: 'modals_comparison'});
          let result = HTTP.call(endpoint.method, endpoint.url, {headers: {"x-api-key": keys.api_key}});
          return result.data;
      } catch (e) {
          // Got a network error, timeout, or HTTP error in the 400 or 500 range.
          console.error(e)
      }
    },
});
