var fs = Npm.require('fs');
var keys = JSON.parse(fs.readFileSync(process.cwd() + '/../web.browser/app/keys.json', 'utf8'));

Meteor.startup(function() {
    console.log("public-dashboard: server startup()");

    var basicAuth = new HttpBasicAuth(function(username, password) {

        return keys.password == password && username == 'admin';
    });
    basicAuth.protect(['/comparativo/saopaulo']);

});
