var sul_names=['parelheiros', 'guarapiranga', 'santo amaro', 'ibirapuera'];
var oeste_names=['pirituba', 'rebouças', 'reboucas'];
var leste_names=['tiradentes', 'paes de barros', 'luiz inácio anhaia melo'];
var norte_names=['inajar', 'engenheiro caetano alvares'];

Template.CarBus.helpers({

    data: function () {
      return Session.get('streetsData')
    },

});

function getTrechoBC(paths) {
  for(var i=0; i<paths.length; i++){
    if(isBC(paths[i].name)) {
      return(paths[i]);
    }
  }
}

function getTrechoCB(paths) {
  for(var i=0; i<paths.length; i++){
    if(isCB(paths[i].name)) {
      return(paths[i]);
    }
  }
}

function isBC(nameWithDirection) {
  if(nameWithDirection.indexOf('B/C')>-1) {
    return(true);
  }
}

function isCB(nameWithDirection) {
  if(nameWithDirection.indexOf('C/B')>-1) {
    return(true);
  }
}

function getNameWithoutDirectionFromNameWithDirection(name) {

  var nameWithoutDirection=name;

  if(name.indexOf('B/C')>-1 || name.indexOf('C/B') > -1) {
      nameWithoutDirection=name.slice(0,-6);
  } else {
    return undefined;
  }

  return(nameWithoutDirection);
}

function getZoneForTrecho(trecho){

  var zone="";

  // checks for sul
  _.each(sul_names, function(tag){
    if(trecho.name.toLowerCase().indexOf(tag)>-1){
      zone='sul';
    }
  });
  if(zone!="")
    return(zone);

  // checks for norte
  _.each(norte_names, function(tag){
    if(trecho.name.toLowerCase().indexOf(tag)>-1){
      zone='norte';
    }
  });
  if(zone!="")
    return(zone);

  // checks for leste
  _.each(leste_names, function(tag){
    if(trecho.name.toLowerCase().indexOf(tag)>-1){
      zone='leste';
    }
  });
  if(zone!="")
    return(zone);

  // checks for oeste
  _.each(oeste_names, function(tag){
    if(trecho.name.toLowerCase().indexOf(tag)>-1){
      zone='oeste';
    }
  });
  if(zone!="")
    return(zone);

  return(null);
}

Template.CarBus.created = function () {
    //console.log(">>> CarBus created");

    Tracker.autorun(function () {

        Meteor.call('get_modals_comparison', Session.get('agency_id'), function(err, trechos) {

          var done = [];
          var paths;

          if (err) {console.error(err)}
          var zones={'sul':[], 'norte':[], 'leste':[], 'oeste':[]};
          var streets={};
          var cardEntity;
          var trechosByName={};
          _.each(trechos, function(trecho){

            var zone=getZoneForTrecho(trecho);

            var name=getNameWithoutDirectionFromNameWithDirection(trecho.name);

            if ((name) && (!done.includes(name))) {

              paths = _.filter(trechos, function(path){
                {return getNameWithoutDirectionFromNameWithDirection(path.name) === name}
              })

              if(paths.length==2){
                var trechoBC=getTrechoBC(paths);
                var hashIndex=name.replace(/\(|\)/g, '');
                trechosByName[hashIndex]={BC: trechoBC, CB: getTrechoCB(paths)};
              }

              if (zone) {
                zones[zone].push(trecho);
                cardEntity = zones;
              } else {
                if (!_.contains(Object.keys(streets), name)) { streets[name] = [] };
                streets[name].push(trecho);
                cardEntity = streets;
              }
            }
          });

          var data=[];
          var zone = undefined;
          var name = undefined;
          var count = 0;

          _.each(_.keys(cardEntity), function(entity){

            if (cardEntity === zones) {
              zone = entity
              name ='ZONA ' + zone.toUpperCase();
            } else {
              name = undefined; // 'name' refers to CARD name [zone] (not path name)
            }

            var bothPaths=[];
            var pathsAdded=[];

            _.each(cardEntity[entity], function(path){
              var pathName=getNameWithoutDirectionFromNameWithDirection(path.name);
              if(pathName!=undefined && !_.contains(pathsAdded, pathName)) {
                var realName = pathName;
                if (pathName.includes('CORREDOR')) {realName = pathName.slice(9)}
                pathsAdded.push(pathName);
                bothPaths.push({name: realName, paths:trechosByName[pathName.replace(/\(|\)/g, '')]});
              }
            })
            data.push({'zone': zone, 'name': name, 'trechos': bothPaths})
          });
          Session.set('streetsData', data);
       })
    });
};

Template.CarBus.destroyed = function () {
    //console.log(">>> CarBus destroyed       ");
};
