Template.generalSpeed.helpers({
  getHelpText: function() {
    return("Comparativo de velocidades<br>"+
           "entre modos de transporte<br>"+
           "neste momento");
  },

  carMeters: function() {
    var carMeters=Session.get('carMeters')/1000;
    return(carMeters.toFixed(0));
  },

  busMeters: function() {
    return((Session.get('busMeters')/1000).toFixed(0));
  },

  jamMeters: function() {
    return((Session.get('jamMeters')/1000).toFixed(0));
  }

});


Template.generalSpeed.rendered = function() {

  Tracker.autorun(function(){

    Meteor.call('get_modals_comparison', Session.get('agency_id'), function(err, trechos) {

      var carMeters=0;
      var carMinutes=0;
      var busMeters=0;
      var busMinutes=0;
      _.each(trechos, function(compare){
        carMeters += compare.car.length ? compare.car.length : 0;
        carMinutes += compare.car.time_minutes ? compare.car.time_minutes : 0;
        busMeters += compare.bus.length ? compare.bus.length : 0;
        busMinutes += compare.bus.time_minutes ? compare.bus.time_minutes : 0;
      });

      Session.set('carMeters', carMeters);
      Session.set('busMeters', busMeters);

      var carAvg=(carMeters/1000)/(carMinutes/60);
      var busAvg=(busMeters/1000)/(busMinutes/60);

      var attr={
        width: 100,
        height: 100,
        speed: busAvg,
        normal: 50,
        divId: "#general-speed-bus"
      }
      createDisc(attr);

      attr={
        width: 100,
        height: 100,
        speed: carAvg,
        normal: 50,
        divId: "#general-speed-car"
      }
      createDisc(attr);

      })
  });
}

function createDisc(attr, sentido) {

  var width = attr.width,
      height = attr.height,
      twoPi = 2 * Math.PI;
  var total=1;
  var speed=attr.speed;
  var percent=attr.speed/attr.normal > 1 ? 1 : (attr.speed/attr.normal).toFixed(2);
  var div=attr.divId;

  var color="#ddd";
  if(speed>=25){
    color="#23c932";
  } else if(speed>=20) {
    color="#f3bd0c";
  } else if(speed>=15) {
    color="#f35d0c";
  } else if(speed < 15){
    color="#cc0a0a";
  }

  var arc = d3.svg.arc()
      .innerRadius(0.3*width)
      .outerRadius(0.4*width)
      .startAngle(0);

  var svgEnter = d3.selectAll(div).selectAll("svg").data([speed.toFixed(1)]).enter();
  var svg = svgEnter.append("svg")
                .attr("width", width)
                .attr("height", height)
              .append("g")
                .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")")

  var meter = svg.append("g")
                .attr("class", "season-progress");

  meter.append("path")
          .datum({endAngle: twoPi})
          .style("fill", "#ddd")
          .attr("d", arc);

  var foreground = meter.append("path")
      .datum({endAngle:0})
      .attr("class", "foreground")
      .style("fill", color)
      .attr("d", arc);

  var text =  meter.append("text")
      .attr("text-anchor", "middle")
      .attr("dy", ".35em")
      .attr("font-size", 0.26*width)
      .attr("class", "speed-disc-text");

  d3.selectAll(div).select("svg").select("text")
      .text(function(d){return d});

  //d3.selectAll(".foreground")

  d3.selectAll(div).select(".foreground")
    .style("fill", color);

  (function transition(element, speed, arc) {
    element.transition()
    .duration(1000)
    .ease("linear")
    .attrTween("d", function(d) {
               var interpolate = d3.interpolate(d.endAngle, -twoPi * percent / total)
               return function(t) {
                  d.endAngle = interpolate(t);
                  return arc(d);
               }
            });
    })(d3.selectAll(div).select(".foreground"), percent, arc);
}
