var m = new Mapa();

var center_coords = {saopaulo_sp: {lat: -23.573108, lng: -46.562165}, montevideo_uy: {lat: -34.898757, lng: -56.165382}};

Template.mapTemplate.events({});

Template.mapTemplate.helpers({

});

Template.mapTemplate.rendered = function () {
    console.log(">>> mapTemplate rendered");

    Tracker.autorun(function () {

        var center = center_coords[Session.get('agency_id')];
        m.init(center.lat, center.lng);

        Meteor.call("get_edges_map", Session.get('agency_id'), function(error, results) {
          if (error) {console.log('ERROR:::', error)}
          
          var jam_length = 0

          for(var i = 0; i < results.length; i++) {
              var curr_speed;
              var edges = results[i].edges;

              for (var j = 0; j < edges.length; j++) {
                var data = edges[j];
                if (results[i].jams) {jam_length = jam_length + dist(data.shape)}
                curr_speed = results[i].jams ? 0 : data.curr_speed ;
                m.drawShape(data.shape, curr_speed, 35);
              }
          }
          Session.set('jamMeters', jam_length*Math.PI*6371000/180);
        });
    });
};

Template.mapTemplate.destroyed = function () {
    console.log(">>> mapTemplate destroyed       ");
};

function dist(shape) {
  let last = null;
  let dist = 0;
  for (let point of shape) {
    if (last) {
      deltaLat = point.loc.lat - last.loc.lat;
      deltaLng = point.loc.lng - last.loc.lng;
      dist = dist + Math.sqrt(Math.pow(deltaLat, 2) + Math.pow(deltaLng, 2))
    }
    last = point
  }
  return dist
}
