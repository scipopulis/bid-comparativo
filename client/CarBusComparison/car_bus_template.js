Template.CarBusTemplate.events({});

Template.CarBusTemplate.helpers({

    formatRound: function (number) {
        if (number === null || number === undefined || isNaN(number)) {
            return "";
        }
        return Math.round(number);
    },
    formatDateFromNow: function(date) {
        return moment(date).fromNow();
    },
    originDetailForKey: function (name) {
        var subtitle = name.split(':')[1];
        var origin   = subtitle.split('-')[0];
        return origin;
    },
    originMainForKey: function (name) {
        var subtitle = name.split(':')[0];
        var origin   = subtitle.split('-')[0];
        return origin;
    },
    destinationDetailForKey: function (name) {
        var subtitle    = name.split(':')[1];
        var destination = subtitle.split('-')[1];
        return destination;
    },
    destinationMainForKey: function (name) {
        var subtitle    = name.split(':')[0];
        var destination = subtitle.split('-')[1];
        return destination;
    },
    classForStopWatch: function (value1, value2) {
        if(Math.pow((value1-value2),2)<4)
          return "amarelo"

        if (value1 < value2)
            return "verde";
        else
            return "vermelho"
    },
    barSize: function(time1, time2) {
      if(time1>time2)
        return 100;
      else {
        return(Math.round((time1/time2)*100));
      }
    }
});

Template.CarBusTemplate.created = function () {
    //console.log(">>> CarBusTemplate.created");
};

Template.CarBusTemplate.destroyed = function () {
    //console.log(">>> CarBusTemplate destroyed       ");
};
